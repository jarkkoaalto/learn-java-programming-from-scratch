package section4;

public class Recap {

	/*
	 * #13 recap - What we've learnt so far ...
	 * - variables
	 * - Mathematical operatiors
	 * - String 
	 * - Methods\functions
	 * - Boolean logic: if, else and else if statement
	 * - Moore boolean logic " and & or
	 * - loops: while and do while
	 * - Arrays
	 * - For loop and enhanced for loop
	 * - intro to OOP
	 * - Constructor
	 */
	public static void main(String[] args) {

		int a = 10;
		int b = 20;
		int c = -1;
		int d = ((a*b*c) + (a+b)-(c*(a+b)));
		System.out.println(d);
		
		int i = 100 % 3;
		System.out.println(i);
	
		String name = "Ollie";
		String hello = "Hello";
		String world = " world";
		String helloworld = hello + world;
		System.out.println(helloworld);
		
		print("Hello, world is ooo");
		System.out.println(add(4,5));
		
		int m = 100;
		int n = 10;
		boolean test = m > n;
		if(test) {
			System.out.println("m is greater than n");
		}else if (m == n) {
			System.out.println("m is equal than n");
		}else {
			System.out.println("m is less than n");
		}
		
		int u = 100;
		int w = -100;
		
		boolean testA = u > 0;
		boolean testB = w > 0;
		if(testA && testB) {
			System.out.println("Both is true");
		}
		if(testA || testB) {
			System.out.println("Both is true");
		}
	
	}
	
	static void print(String s) {
		System.out.println(s);
	}
	
	static int add(int a, int b) {
		return a + b;
	}
}

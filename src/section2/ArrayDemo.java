package section2;

public class ArrayDemo {

	public static void main(String[] args) {
		
		String [] namesOfWorkers = {"Bill","Bob", "Ollie", "Jared", "Lena","Tina"};
		for(int i= 0; i<namesOfWorkers.length; i++) {
			System.out.println(namesOfWorkers[i]);
		}
		
		System.out.println("");
		String [] Workers = new String[4];
		Workers[0] = "Jaska";
		Workers[1] = "Jaana";
		Workers[2] = "Pekka";
		Workers[3] = "Ollie";
		
		for(int i = 0; i<Workers.length; i++) {
			System.out.println(Workers[i]);
		}
		

	}

}

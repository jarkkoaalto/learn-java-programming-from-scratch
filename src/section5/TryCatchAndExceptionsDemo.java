package section5;

import java.io.IOException;

public class TryCatchAndExceptionsDemo {
	public static void main(String[]args) {
		int [] array = {0,1};
		
		
		try {
			System.out.println(array[1]);
		}catch(Exception e) {
			System.err.println("Could not access array elemnet");
			e.printStackTrace(System.err);
		}
		
		
	}

}

package section2;



/*
 * # 5. Methods
 */
public class MethodsDemo {

	public static void main(String[] args) {
		
		int i = 0;
		while(i<10) {
			System.out.print(i + " ");print();
			i++;
		}
		
		
		int answer = add(10,5);
		System.out.println(answer);
	}
	
	
	// return-type,identifier, parameters
	static void print() {
		System.out.println("Hello, world");
	}
	
	
	static void printing(String s) {
		System.out.println(s);
	}
	
	static int add(int u, int l) {
		int k = u + l;
		return k;
	}

}

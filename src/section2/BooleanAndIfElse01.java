package section2;

public class BooleanAndIfElse01 {

	public static void main(String[] args) {
		
		int a = 10;
		int b = 20;
		
		boolean test = a > b;
		System.out.println(test);

		boolean jos = false;
		
		if(!jos) {
			System.out.println("test is true");
		}else {
			System.out.println("test is false");
		}
		
		
		int y = 10;
		int  u = 20;
		
		
		if(y > u) {
			System.out.println("y is greater than u");
		}else if(y == u) {
			System.out.println("y is equal to p");
		}else {
			System.out.println("y is less than u");
		}
		
	}

}

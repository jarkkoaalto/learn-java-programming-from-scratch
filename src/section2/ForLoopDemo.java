package section2;

/*
 * # 10. For loop and enched for loop
 * 
 */
public class ForLoopDemo {

	public static void main(String[] args) {
		String[] names = {"Ollie", "Alex", "Ellie","Jason"};
		
		// Variable; Condision; increment/decrement
		for(int i = 0; i < names.length; i++) {
			System.out.println(names[i]);
		}
		
		System.out.println();
		// Enhabced loop
		for(String name : names) {
			System.out.println(name);
			
		}
	}

}

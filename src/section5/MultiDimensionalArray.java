package section5;

public class MultiDimensionalArray {
	public static void main(String [] args) {
		
		
		float [][][] coordinates = new float[16][16][16];
		
		for(int i = 0; i < 16; i++) {
			System.out.println("First dimension: " + i);
			for(int j = 0; j < 16; j++) {
				System.out.println("Second dimension: " + j);
				for(int n = 0;n < 16; n++) {
					System.out.println("\t\tThird Dimension: " + n);
					coordinates[i][j][n] = 3.14f;
				}
			}
			
		}
	}

}

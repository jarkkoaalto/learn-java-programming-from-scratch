package section2;

public class StringAndMathematicalOperators {

	public static void main(String[] args) {
		String helloworld  ="Hello, world !";
		System.out.println(helloworld);

		int a = 10;
		int b = 20;
		
		int c = b + a;
		int s = b *  a;
		
		int n = (a*b) - 3;
		int m = n * n;
		
		
		System.out.println(b + " + " + a + " = " + c );
		System.out.println(b + " * " + a + " = " + s);
		System.out.println(b + " * " + a + " - " + 3 + " = "+ n);
		System.out.println(n + " * " + n + " = " + m);
		
		
		String hello = "Hello ";
		String world = " world";
		String hellowo = hello + world;
		System.out.println(hellowo);
	}

}

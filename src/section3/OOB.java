package section3;

public class OOB {

	public static void main(String[] args) {
		Person person1 = new Person();
		Person person2 = new Person();
		Person person3 = new Person();
		
		person1.name = "Ollie";
		person1.age = 17;
		person1.dob = "17/03/95";
		
		person2.name = "Jared";
		person2.age = 20;
		person2.dob = "15/01/90";
		
		person3.name = "Tim";
		person3.age = 30;
		person3.dob ="26/06/80";
		
		System.out.println("Person 1 :\n" + "Name: " + person1.name + "\n" 
				+ "Age: " + person1.age + "\n" + "Date of birth" + person1.dob + "\n");

		
		System.out.println("Person 2 :\n" + "Name: " + person2.name + "\n" 
				+ "Age: " + person2.age + "\n" + "Date of birth" + person2.dob + "\n");
		
		System.out.println("Person 3 :\n" + "Name: " + person3.name + "\n" 
				+ "Age: " + person3.age + "\n" + "Date of birth" + person3.dob + "\n");
	}

}

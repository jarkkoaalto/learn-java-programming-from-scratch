package section5;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class FileReadingDemo {

	public static void main(String[] args) throws IOException {
		/*
		BufferedWriter bw = new BufferedWriter(new FileWriter("C:/Users/Jarkko/Desktop/test.txt"));
		bw.write("Hello world text file");
		bw.newLine();
		bw.write("This is a test ...");
		
		bw.close();
		*/
		
		BufferedReader read = new BufferedReader(new FileReader("C:/Users/Jarkko/Desktop/test.txt"));
		String line;
		while((line=read.readLine()) !=null) {
			System.out.println(line);
		}
	
	}

}

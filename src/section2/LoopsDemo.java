package section2;

public class LoopsDemo {

	public static void main(String[] args) {
	
		int x = 0;
		while(x < 100) {
			System.out.print("-");
			x++;
		}
		
		System.out.println();
		int t = 1;
		int total = 0;
		
		while(t <= 100) {
			if(t %2 == 0) {
				total += t;
				if(total > 100) {
					break;
				}
			}
			t++;
		}
		System.out.println("The total is " + total);
	
		int o = 90;
		
		do {
			System.out.println(o);
		}while(o < 100);
		o++;
		
	}

}

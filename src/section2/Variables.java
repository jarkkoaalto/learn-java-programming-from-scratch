package section2;

public class Variables {
	public static void main(String [] args) {
		/*
		 * Variable-type, indentifier;assignment-operator, value
		 * 
		 */
		byte myB = 23;  // 8 bits 
		short mys = 3;	// 16 bits	
		int a = 25; // 32 bits
		long mylong = 26856; // 64 bits
		
		float myfloat= 4.5f;
		double mydouble = 4.5d;
		System.out.println(myB);
		System.out.println(mys);
		System.out.println(a);
		System.out.println(mylong);
		System.out.println(myfloat);
		System.out.println(mydouble);
		
	}

}

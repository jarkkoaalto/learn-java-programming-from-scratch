package section2;

public class BooleanAndIfElse02 {
	public static void main(String []args) {
		int a = 10;
		int b = 20;
		int c = 30;
		
		// AND function
		boolean test = (a < b) && (b > c);
		System.out.println(test);
		
		//OR function
		boolean test1 = (a < b) || (b > c);
		System.out.println(test1);
		
		// MiX function
		boolean test2 = (a < b) && ((b > c) || (b + a == c));
		System.out.println(test2);
	}

}

package section5;

public class SwitchEnumerationDemo {
	
	
	enum Days{
		MONDAY, TUESDAY, WEDNESDAY,THRSDAY, FRIDAY, SATURDAY, SUNDAY;
	}
	
	
	public SwitchEnumerationDemo() {
	
		
		int a = 5;
		switch(a) {
		case 10:
			System.out.println("Integer a is 10");
			break;
		case 5:
			System.out.println("Integer a is 5");
			break;
		default:
			System.out.println("A is not a valid value");
			break;
		
		}
		
	}
	
	
	public static void main(String [] args) {
		new SwitchEnumerationDemo();
	}

}

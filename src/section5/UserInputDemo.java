package section5;

import java.util.Scanner;


public class UserInputDemo {

	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		System.out.println("What is your name:");
		String name = reader.nextLine();
		
		
		// System.out.println("Your name is : " + name);
		System.out.println("How old are you ? ");
		
		int age = reader.nextInt();
		
		System.out.println("Your name is : " + name + " you are " + age + " years old");
	
	boolean gotIt = false;
	while(!gotIt) {
		try {
			age = reader.nextInt();
			gotIt = true;
			}catch(Exception e) {
				System.out.println("That is not a valid number! ");
				reader.next();
				continue;
			}
	}
	
	}

}

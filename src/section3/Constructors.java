package section3;

public class Constructors {
	public static void main(String[] args) {
		Henkilo hlo1 = new Henkilo("Olki","25/94/1971",56);
		Henkilo hlo2 = new Henkilo("Jared","01/10/1989",45);
		Henkilo hlo3 = new Henkilo("Tim","03/02/1932", 100);
		
		System.out.println("Person 1 :\n" + hlo1.name + " " + hlo1.dob + " " + hlo1.age);
		System.out.println("Person 2 :\n" + hlo2.name + " " + hlo2.dob + " " + hlo2.age);
		System.out.println("Person 3 :\n" + hlo3.name + " " + hlo3.dob + " " + hlo3.age);
		
	}
}
